module.exports = {
  content: ["./index.html","./success.html"],
  theme: {
    extend: {
      colors: {
        'brand-red':"#BF0F0F",
        'brand-blue-light': "#1c1a53",
        'brand-blue-dark': "#0b1035"
      },
      fontFamily: {
        'heading': ['new-spirit-condensed','serif'],
        'heading-bold': ['new-spirit','serif'],
        'body' : ['Roboto', 'sans-serif']
      },
      backgroundImage: {
        'hero': "url('/images/hero-bg.jpg')",
        'intro': "url('/images/fireworks-vector-bg.jpg')",
        'temp-map': "url('/images/temp-map.jpg')",
        'flag': "url('/images/flag-bg.jpg')",
        'hero-simple': "url('/images/hero-simple.jpg')",
        'map-1': "url('/images/maps/map-1.png')",
        'map-2': "url('/images/maps/map-2.png')",
        'map-3': "url('/images/maps/map-3.png')",
        'map-4': "url('/images/maps/map-4.png')",
        'map-5': "url('/images/maps/map-5.png')",
        'map-6': "url('/images/maps/map-6.png')",
        'map-7': "url('/images/maps/map-7.png')",
        'map-8': "url('/images/maps/map-8.png')",
        'map-9': "url('/images/maps/map-9.png')",
        'map-10': "url('/images/maps/map-10.png')",
      }
    },
  },
  plugins: [],
}
